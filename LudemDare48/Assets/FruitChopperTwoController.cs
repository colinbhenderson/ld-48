﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitChopperTwoController : MonoBehaviour
{
    public GameObject TimerBar;
    public GameObject InventoryManager;

    private bool mixerInUse = false;

    void Update()
    {
        //Checks to see if the timer is done - Once done, reset timer and execute inventory function
        bool timerCheck = TimerBar.GetComponent<TimerScript>().TimerIsDone;



        if (timerCheck)
        {
            TimerBar.GetComponent<TimerScript>().SetTimerOff();
            mixerInUse = false;
            InventoryManager.GetComponent<InventoryManager>().AddItem("ChoppedPineapple", 1);
        }

        else if (!mixerInUse)
        {
            AutoWorker();
        }
    }


    public void AutoWorker()
    {
        //If there is an item to be processed - Do it
        if (InventoryManager.GetComponent<InventoryManager>().Pineapple > 0)
        {
            mixerInUse = true;
            InventoryManager.GetComponent<InventoryManager>().RemoveItem("Pineapple", 1); //Removes item from Inventory
            TimerBar.GetComponent<TimerScript>().TimerStart(); //Starts Timer Animation
        }
    }
}
