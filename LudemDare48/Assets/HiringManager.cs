﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiringManager : MonoBehaviour
{
    public InventoryManager inventoryManager;
    public int MaxBasicWorkers = 5;
    public GameObject basicWorkerPrefab;
    public GameObject chopperWorkerPrefab;
    public GameObject boardWorkerPrefab;
    public GameObject slicerWorkerPrefab;
    public GameObject mixerWorkerPrefab;
    public GameObject cookWorkerPrefab;

    public GameObject MixerTwoContainer;
    public GameObject SlicerTwoContainer;
    public GameObject BoardTwoContainer;
    public GameObject ChopperTwoContainer;



    public int wageBasic;
    public int initalCostBasic;
    public int wageSkilled;
    public int initialCostSkilled;
    // Start is called before the first frame update
    void Start()
    {
        inventoryManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
        wageBasic = 15;
        wageSkilled = 30;
        initalCostBasic = 50;
        initialCostSkilled = 100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void addBasicWorker()
    {
        if (inventoryManager.BasicWorker < MaxBasicWorkers)
        {
            inventoryManager.AddItem("BasicWorker", 1);
            inventoryManager.RemoveMoney(initalCostBasic);
        }
    }

    public void removeBasicWorker()
    {
        if (inventoryManager.BasicWorker > 0)
            inventoryManager.RemoveItem("BasicWorker", 1);
    }

    public void addChopperWorker()
    {
        if (inventoryManager.ChopperWorker < 1)
        {
            inventoryManager.AddItem("ChopperWorker", 1);
            inventoryManager.RemoveMoney(initialCostSkilled);
        }
    }

    public void removeChopperWorker()
    {
        if (inventoryManager.ChopperWorker > 0)
            inventoryManager.RemoveItem("ChopperWorker", 1);
    }

    public void addShredderWorker()
    {
        if (inventoryManager.ShredderWorker < 1)
        {
            inventoryManager.AddItem("ShredderWorker", 1);
            inventoryManager.RemoveMoney(initialCostSkilled);
        }
    }

    public void removeShredderWorker()
    {
        if (inventoryManager.ShredderWorker > 0)
            inventoryManager.RemoveItem("ShredderWorker", 1);
    }

    public void addSlicerWorker()
    {
        if (inventoryManager.SlicerWorker < 1)
        {
            inventoryManager.AddItem("SlicerWorker", 1);
            inventoryManager.RemoveMoney(initialCostSkilled);
        }
    }

    public void removeSlicerWorker()
    {
        if (inventoryManager.SlicerWorker > 0)
            inventoryManager.RemoveItem("SlicerWorker", 1);
    }

    public void addMixerWorker()
    {
        if (inventoryManager.MixerWorker < 1)
        {
            inventoryManager.AddItem("MixerWorker", 1);
            inventoryManager.RemoveMoney(initialCostSkilled);
        }
    }

    public void removeMixerWorker()
    {
        if (inventoryManager.MixerWorker > 0)
            inventoryManager.RemoveItem("MixerWorker", 1);
    }


    // NEEDS SOME SORT OF CONTAINER FOR EACH INSTANTIATION

    public void assignBasicWorker(GameObject container)
    {
        GameObject temp = Instantiate(basicWorkerPrefab, container.transform);
        temp.transform.localPosition = new Vector3(0,0,0);
    }

    public void unassignBasicWorker(GameObject container)
    {
        GameObject.Destroy(container.transform.GetChild(0).gameObject);
    }


}
