﻿    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;

    public class MixerController : MonoBehaviour, IDropHandler
    {

    void Update()
    {
        //Checks to see if the timer is done - Once done, reset timer and execute inventory function
        bool timerCheck = TimerBar.GetComponent<TimerScript>().TimerIsDone;
        if (timerCheck)
        {
            TimerBar.GetComponent<TimerScript>().SetTimerOff();
            Debug.Log("Adding One Dough Ball!!");
            mixerInUse = false;
            InventoryManager.GetComponent<InventoryManager>().AddItem("DoughBall", 1);
            InventoryManager.GetComponent<InventoryManager>().BasicWorker += 1;
            HiringManager.GetComponent<HiringManager>().unassignBasicWorker(WorkerContainer);
        }
    }



    public GameObject TimerBar;
    public GameObject InventoryManager;
    public GameObject WorkerContainer;
    public GameObject HiringManager;

    private bool mixerInUse = false;


    
    public void OnDrop(PointerEventData eventData)
    {
        //Gets the item that is attatched to the pointer
        RectTransform item = eventData.pointerDrag.GetComponent<RectTransform>();

        //Checks to see if the mixer is in use
        if (mixerInUse)
        {
            //TODO: Something to tell the player that mixer is being used
            Debug.Log("Opps! Already Working!");
        }

        //Checks to see if the pointer has an item and its the correct one
        else if (eventData.pointerDrag != null && item.name == "DoughMix" && WorkerCheck())
        {
            mixerInUse = true;
            InventoryManager.GetComponent<InventoryManager>().RemoveItem("DoughMix", 1); //Removes item from Inventory
            TimerBar.GetComponent<TimerScript>().TimerStart(); //Starts Timer Animation
        }

        //An item that does not work was palced here
        else
        {
            //TODO: SOMETHING TO INDICATE YOU PUT THE ITEM IN THE WRONG PLACE
            Debug.Log("Nope, not here...");
        }
    }

    public bool WorkerCheck()
    {
        if (WorkerContainer.transform.childCount < 1 && InventoryManager.GetComponent<InventoryManager>().BasicWorker > 0)
        {
            InventoryManager.GetComponent<InventoryManager>().BasicWorker -= 1;
            HiringManager.GetComponent<HiringManager>().assignBasicWorker(WorkerContainer);
            return true;
        }
        else
        {
            return false;
        }
    }





}
