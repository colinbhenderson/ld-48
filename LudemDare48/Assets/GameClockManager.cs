﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameClockManager : MonoBehaviour
{
    public float gameTime;
    public float minutesInADay;
    public float daysInAMonth;
    public float days;
    public float daysPassed;
    public float minutesPassed;
    public float monthsPassed;
    public float counter;

    public int DayNumber;
    public int MonthNumber;
    public int DayTime = 3;
    public int customersServed;
    public int moneyMade;
    public int wagesDue;

    private int numSkilledWorkers;
    private int rent;
    private int totalProfit;

    public bool GameIsRunning = false;
    public bool DayRecapOpen = false;

    public Text CustomersServed;
    public Text MoneyMade;
    public Text WagesDue;
    public Text RentDue;
    public Text TotalProfit;
    public Text date;
    public Text MonthText;
    public Text DayText;

    public CustomerManager customerManager;
    public Animator recapAnimator;
    public InventoryManager inventoryManager;
    public HiringManager hiringManager;
    public GameObject TimerBar;

    private bool clockIsOn = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Wait for Start button to be pressed 
        //Offer to play tutorial 
        //Start game on M1 D1
        //Run game until end of D1
        //Reset
        //Start M1 D2
        //Repeat Stpes 3-7

        //By default Timer scrip is inactive, only turned on if Tutorial is skipped or after completed
        //Once Clock reaches end of day - Pause clock, reset clock, open day recap screen
        //Once player confirms day recap and finishes shopping, update Day/Month counter and start clock again? 

        numSkilledWorkers = (inventoryManager.ChopperWorker + inventoryManager.MixerWorker
            + inventoryManager.ShredderWorker + inventoryManager.SlicerWorker);

        CustomersServed.text = customersServed.ToString();
        MoneyMade.text = "$ " + moneyMade.ToString() + ".00";
        WagesDue.text = "$ " + wagesDue.ToString() + ".00";
        RentDue.text = "$ " + rent.ToString() + ".00";
        TotalProfit.text = "$ " + totalProfit.ToString() + ".00";

        if (minutesPassed >= DayTime && !DayRecapOpen)
        {
            DayRecapOpen = true;

            //Pause clock, reset minPassed Clock | open day recap screen
            
            
            //Open Recap Screen
            //Do the menu stuff, On they move to next day do this
            wagesDue = (numSkilledWorkers * hiringManager.wageSkilled) + (inventoryManager.BasicWorker * hiringManager.wageBasic);

            //Stops the clock animation
            TimerBar.GetComponent<GameTimer>().SetTimerOff();
            clockIsOn = false;
            recapAnimator.Play("RecapSlideIn");

            if ((wagesDue + rent) > inventoryManager.Money)
            {
                Debug.LogError("GAME HAS BEEN LOST U SUCK!");
            }

            if (daysPassed == 9)
            {
                monthsPassed++;
                daysPassed = 0;
                rent = 200;
            }

            totalProfit = moneyMade - (wagesDue + rent);

            date.text = (monthsPassed + 1).ToString() + " / " + (daysPassed + 1).ToString();
            UpdateDay();
            UpdateMonth();
        }
        else if (minutesPassed < DayTime && !DayRecapOpen)
        {
            if (!clockIsOn)
            {
                StartVisualClock();
            }
            //Run the visual timer to show that time is passing
            gameClock();
            //Any other logic that needs to run during the day
            customerCounter(30);
            UpdateDay();
            UpdateMonth();
        }
            //Pause clock, reset minPassed Clock | open day recap screen

            //Some Logic to check if last day in month 

            //Some Logic to check if last month in game 
            //Check if Deepest Dish Pizza unlocked by this point
            //If yes - you win | else you lose 
        
    }

    public void setRecapToFalse()
    {
        recapAnimator.Play("RecapSlideOut");
        DayRecapOpen = false;
        inventoryManager.RemoveMoney(wagesDue + rent);
        rent = 0;
        gameTime = 0;
        minutesPassed = 0;
        daysPassed++;
        customerManager.closeAllOrders();
        //Stop Auto Worker
        //Delete all Pizzas
        //Clear Pizza Bench
    }

    public void goBackToStartScreen()
    {
        SceneManager.LoadScene("Start Screen");
    }



    private void gameClock()
    {
        gameTime += Time.deltaTime;
        //Tracks how many minutes have passed since the start of the day
        minutesPassed = (int)(gameTime / 60.0f);

        //ARCHIVE:
        //daysPassed = (int)(minutesPassed / minutesInADay);
        //monthsPassed = (int)(daysPassed / daysInAMonth);
    }


    //Makes a new order every X seconds
    private void customerCounter(int orderFrequency)
    {
        counter += Time.deltaTime;

        if ((int)counter > orderFrequency)
        {
            Debug.Log("Called");
            customerManager.spawnCustomer();
            counter = 0;
        }
    }


    public void StartGame()
    {
        GameIsRunning = true;
    }

    public void StopGame()
    {
        GameIsRunning = false;
    }

    public void EndDayRecap()
    {
        DayRecapOpen = false;

    }

    public void StartVisualClock()
    {
        clockIsOn = true;
        //TODO: This needs to be updated to accuratly reflect the time remaining
        TimerBar.GetComponent<GameTimer>().SetTime(DayTime * 60);
        TimerBar.GetComponent<GameTimer>().TimerStart(); //Starts Timer Animation

    }

    public void UpdateDay()
    {
        int total = (int)daysPassed + 1;
        DayText.text = "Day: " + total;
    }

    public void UpdateMonth()
    {
        int total = (int)monthsPassed + 1;
        MonthText.text = "Month: " + total;
    }


}
