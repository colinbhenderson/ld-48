﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GarbageCanController : MonoBehaviour, IDropHandler
{
    public GameObject InventoryManager;
    public GameObject PizzaBench;

    [SerializeField] private Canvas canvas;
    private CanvasGroup canvasGroup;

    public void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        //Gets the item that is attatched to the pointer
        RectTransform item = eventData.pointerDrag.GetComponent<RectTransform>();

        if(item.GetComponent<PizzaObject>() != null)
        {
            PizzaBench.transform.GetComponent<PizzaWorkbenchController>().pizzaOnTable = false;
            GameObject.Destroy(item.transform.gameObject);
        }

    }
}
