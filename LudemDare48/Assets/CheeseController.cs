﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class CheeseController : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField] private Canvas canvas;

    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    public Vector2 StartingPos;
    public GameObject InventoryManager;


    public void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        StartingPos = this.transform.position;
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = .5f;
        canvasGroup.blocksRaycasts = false;
    }

    //Logic that executes during drag opperation
    public void OnDrag(PointerEventData eventData)
    {
        if (InventoryManager.GetComponent<InventoryManager>().GetItemAmt("Cheese") > 0)
        {
            //Attatches the item to mouse
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor; //Allows for UI scaling
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1f;
        this.transform.position = StartingPos;


    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");

    }

}
