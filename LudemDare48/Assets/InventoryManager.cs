﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Money = 1000;
    }


    public int DoughMix;
    public int Cheese;
    public int Pepperoni;
    public int Pineapple;
    public int DoughBall;    
    public int ShreadedCheese;
    public int SlicedPepperoni;
    public int ChoppedPineapple;
    public int Sauce;
    public int Money;


    public Text DoughMixText;
    public Text CheeseText;
    public Text PepperoniText;
    public Text PineappleText;
    public Text DoughBallText;
    public Text ShreadedCheeseText;
    public Text SlicedPepperoniText;
    public Text ChoppedPineappleText;
    public Text SauceText;
    public Text MoneyText;

    //Workers
    public Text BasicWorkerText;
    public Text ChopperWorkerText;
    public Text BoardWorkerText;
    public Text SlicerWorkerText;
    public Text MixerWorkerText;

    public int BasicWorker;
    public int ChopperWorker;
    public int ShredderWorker;
    public int SlicerWorker;
    public int MixerWorker;


    //TODO:
    //-Keep track of each item
    //Have item Adders/Subtracots for each
    //Have 'Get Value' function for each 
    //Do we do this in an array or a set of Int's? 

    
    // Update is called once per frame
    void Update()
    {
        DoughMixText.text = DoughMix.ToString();
        CheeseText.text = Cheese.ToString();
        PepperoniText.text = Pepperoni.ToString();
        PineappleText.text = Pineapple.ToString();
        DoughBallText.text = DoughBall.ToString();
        ShreadedCheeseText.text = ShreadedCheese.ToString();
        SlicedPepperoniText.text = SlicedPepperoni.ToString();
        ChoppedPineappleText.text = ChoppedPineapple.ToString();
        SauceText.text = Sauce.ToString();
        MoneyText.text = "$" + Money.ToString();
        BasicWorkerText.text = BasicWorker.ToString();
        ChopperWorkerText.text = ChopperWorker.ToString();
        BoardWorkerText.text = ShredderWorker.ToString();
        SlicerWorkerText.text = SlicerWorker.ToString();
        MixerWorkerText.text = MixerWorker.ToString();
    }

    public void AddMoney(int amt)
    {
        this.Money += amt;
    }
    
    public void RemoveMoney(int amt)
    {
        this.Money -= amt;
    }


    //Adds X number of item
    public void AddItem(string itemType, int amt)
    {
        switch (itemType)
        {
            case ("DoughMix"):
                DoughMix += amt;
                break;
            case ("Cheese"):
                Cheese += amt;
                break;
            case ("Pepperoni"):
                Pepperoni += amt;
                break;
            case ("Pineapple"):
                Pineapple += amt;
                break;
            case ("DoughBall"):
                DoughBall += amt;
                break;
            case ("ShreadedCheese"):
                ShreadedCheese += amt;
                break;
            case ("SlicedPepperoni"):
                SlicedPepperoni += amt;
                break;
            case ("ChoppedPineapple"):
                ChoppedPineapple += amt;
                break;
            case ("Sauce"):
                Sauce += amt;
                break;
            case ("BasicWorker"):
                BasicWorker += amt;
                break;
            case ("ChopperWorker"):
                ChopperWorker += amt;
                break;
            case ("ShredderWorker"):
                ShredderWorker += amt;
                break;
            case ("SlicerWorker"):
                SlicerWorker += amt;
                break;
            case ("MixerWorker"):
                MixerWorker += amt;
                break;
            default:
                Debug.LogError("Error: Can't find Inventory Item to Add");
                break;
        }
    }


    //Removes X number of item
    public void RemoveItem(string itemType, int amt)
    {
        switch (itemType)
        {
            case ("DoughMix"):
                DoughMix -= amt;
                break;
            case ("Cheese"):
                Cheese -= amt;
                break;
            case ("Pepperoni"):
                Pepperoni -= amt;
                break;
            case ("Pineapple"):
                Pineapple -= amt;
                break;
            case ("DoughBall"):
                DoughBall -= amt;
                break;
            case ("ShreadedCheese"):
                ShreadedCheese -= amt;
                break;
            case ("SlicedPepperoni"):
                SlicedPepperoni -= amt;
                break;
            case ("ChoppedPineapple"):
                ChoppedPineapple -= amt;
                break;
            case ("Sauce"):
                Sauce -= amt;
                break;
            case ("BasicWorker"):
                BasicWorker -= amt;
                break;
            case ("ChopperWorker"):
                ChopperWorker -= amt;
                break;
            case ("ShredderWorker"):
                ShredderWorker -= amt;
                break;
            case ("SlicerWorker"):
                SlicerWorker -= amt;
                break;
            case ("MixerWorker"):
                MixerWorker -= amt;
                break;
            default:
                Debug.LogError("Error: Can't find Inventory Item to Remove");
                break;
        }
    }

    //Returns the total number of the request item stored in inventory
    public int GetItemAmt(string itemType)
    {
        switch (itemType)
        {
            case ("DoughMix"):
                return DoughMix;
            case ("Cheese"):
                return Cheese;
            case ("Pepperoni"):
                return Pepperoni;
            case ("Pineapple"):
                return Pineapple;
            case ("DoughBall"):
                return DoughBall;
            case ("ShreadedCheese"):
                return ShreadedCheese;
            case ("SlicedPepperoni"):
                return SlicedPepperoni;
            case ("ChoppedPineapple"):
                return ChoppedPineapple;
            case ("Sauce"):
                return Sauce;
            case ("BasicWorker"):
                return BasicWorker;
            case ("ChopperWorker"):
                return ChopperWorker;
            case ("ShredderWorker"):
                return ShredderWorker;
            case ("SlicerWorker"):
                return SlicerWorker;
            case ("MixerWorker"):
                return MixerWorker;
            default:
                Debug.LogError("Error: Can't find Inventory Item to Get Value Of");
                return 0;
        }
    }




}
