﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DeepOvenController : MonoBehaviour, IDropHandler
{
    public GameObject TimerBar;
    public GameObject InventoryManager;
    public GameObject PizzaParent;
    public GameObject PizzaPapa;
    public GameObject InOvenContainer;
    public Animator DoneIndicator;


    [SerializeField] private Canvas canvas;
    private CanvasGroup canvasGroup;

    private bool ovenInUse = false;

    public void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    void Update()
    {

        //Waits to make the oven interactable again - until the pizza is finished cooking 
        if (InOvenContainer.transform.childCount == 0)
        {
            canvasGroup.blocksRaycasts = true;
            canvasGroup.interactable = true;
            //DeActicvate Done Animation
            if (DoneIndicator.GetCurrentAnimatorStateInfo(0).length < 1)
            {
                DoneIndicator.gameObject.SetActive(false);
            }
        }

        //Checks to see if the timer is done - Once done, reset timer and execute inventory function
        bool timerCheck = TimerBar.GetComponent<TimerScript>().TimerIsDone;
        if (timerCheck)
        {
            //Turns the timer off and lets the oven accept more pizza
            TimerBar.GetComponent<TimerScript>().SetTimerOff();
            ovenInUse = false;
            //Activate Done Animation
            Debug.Log("Made it to Animation");
            DoneIndicator.gameObject.SetActive(true);
            DoneIndicator.Play("FlashIndicator");

        }
    }


    public void OnDrop(PointerEventData eventData)
    {
        //Gets the item that is attatched to the pointer
        RectTransform item = eventData.pointerDrag.GetComponent<RectTransform>();

        //Checks to see if the oven is in use
        if (ovenInUse)
        {
            //TODO: Something to tell the player that mixer is being used
            Debug.Log("Opps! Already Working!");
        }

        //Checks to see if the pointer has an item and its the correct one
        else if (eventData.pointerDrag != null && item.name.Contains("Pizza") && item.transform.GetComponent<PizzaObject>().panType == 1)
        {
            ovenInUse = true;
            TimerBar.GetComponent<TimerScript>().TimerStart(); //Starts Timer Animation

            //Change the Starting position to be here.
            item.transform.GetComponent<PizzaObject>().SetNewStartPos(this.transform.position);
            //Moves the pizza into the oven container
            item.transform.SetParent(InOvenContainer.transform);
            //Sets the Pizza to be cooked
            item.transform.GetComponent<PizzaObject>().IsCooked = true;

            //Make the pizza grabable from the oven
            canvasGroup.blocksRaycasts = false;
            canvasGroup.interactable = false;
        }
        //An item that does not work was palced here
        else
        {
            //TODO: SOMETHING TO INDICATE YOU PUT THE ITEM IN THE WRONG PLACE
            Debug.Log("Nope, not here...");
        }
    }
}
