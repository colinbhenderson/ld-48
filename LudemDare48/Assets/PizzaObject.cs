﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PizzaObject : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
 

    public int cheeseCount = 0;
    public int pepperoniCount = 0;
    public int pineappleCount = 0;
    public int doughCount = 0;
    public int sauceCount = 0;
    public int panType = 0;
    public bool IsCooked = false;
    public bool Movable = false;


    [SerializeField] private Canvas canvas;

    private RectTransform rectTransform;
    private CanvasGroup canvasGroup;

    public Vector2 StartingPos;


    // Start is called before the first frame update
    void Start()
    {
        canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddToppings(int cheese, int sauce, int dough, int pepperoni, int pineapple, int pan)
    {
        Debug.Log("--MADE IT--");
        cheeseCount = cheese;
        sauceCount = sauce;
        doughCount = dough;
        pepperoniCount = pepperoni;
        pineappleCount = pineapple;
        panType = pan;
    }



    //----------------------------------------------


    public void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        StartingPos = this.transform.position;
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = .5f;
        canvasGroup.blocksRaycasts = false;
    }

    //Logic that executes during drag opperation
    public void OnDrag(PointerEventData eventData)
    { 
        if (Movable)
        {
            rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor; //Allows for UI scaling
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1f;
        this.transform.position = StartingPos;


    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");

    }

    public void SetNewStartPos(Vector2 pos)
    {
        StartingPos = pos;
    }




}
