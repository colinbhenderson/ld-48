﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CustomerManager : MonoBehaviour
{
    public GameObject orderPrefab;
    public GameObject orderContainer;
    public int numCustomerOrders;
    public int totalOrdersAtOneTime;
    public int pan;
    public GameObject deepPan, deepestPan;
    public InventoryManager inventoryManager;
    // Start is called before the first frame update
    void Start()
    {
        spawnCustomer();
        //inventoryManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
    }

    
    public void spawnCustomer()
    {
        if (numCustomerOrders < totalOrdersAtOneTime)
        {
            GameObject order = Instantiate(orderPrefab, orderContainer.transform);

            if (pan == 0)
            {
                generatePizzaOrder(0, order);
            }

            if (pan == 1)
            {
                int temp = Random.Range(0,1);
                generatePizzaOrder(temp, order);
            }

            if (pan == 2)
            {
                int temp = Random.Range(0, 2);
                generatePizzaOrder(temp, order);
            }

            numCustomerOrders += 1;
            
        }
    }

    public void subtractOrder()
    {
        numCustomerOrders -= 1;
    }

    public void closeAllOrders()
    {
        foreach(Transform child in orderContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
            subtractOrder();
        }
    }

    public void addDeepPanLevel()
    {
        pan = 1;
        deepestPan.GetComponent<CanvasGroup>().interactable = true;
        inventoryManager.RemoveMoney(1000);
    }

    public void addDeepestPanLevel()
    {
        pan = 2;
        inventoryManager.RemoveMoney(3000);
    }

    public void generatePizzaOrder(int pansize, GameObject order)
    {
        int randomInt = Random.Range(1,6);
        Debug.Log(randomInt.ToString());

        OrderObject pizza = order.GetComponent<OrderObject>();

        if (pansize == 0)
        {
            switch (randomInt)
            {
                case 1:
                    pizza.cheeseCount = 1;
                    pizza.doughCount = 1;
                    pizza.sauceCount = 1;
                    pizza.panType = 0;
                    break;
                case 2:
                    pizza.cheeseCount = 1;
                    pizza.doughCount = 1;
                    pizza.pepperoniCount = 1;
                    pizza.panType = 0;
                    break;
                case 3:
                    pizza.cheeseCount = 1;
                    pizza.doughCount = 1;
                    pizza.sauceCount = 1;
                    pizza.pineappleCount = 1;
                    pizza.panType = 0;
                    break;
                case 4:
                    pizza.cheeseCount = 1;
                    pizza.doughCount = 1;
                    pizza.sauceCount = 1;
                    pizza.panType = 0;
                    break;
                case 5:
                    pizza.cheeseCount = 1;
                    pizza.pepperoniCount = 1;
                    pizza.sauceCount = 1;
                    pizza.panType = 0;
                    break;
                case 6:
                    pizza.cheeseCount = 1;
                    pizza.pineappleCount = 1;
                    pizza.sauceCount = 1;
                    pizza.panType = 0;
                    break;
                default:
                    pizza.doughCount = 1;
                    pizza.panType = 0;
                    break;
            }
        }

        if (pansize == 1)
        {
            switch (randomInt)
            {
                case 1:
                    pizza.cheeseCount = 2;
                    pizza.doughCount = 2;
                    pizza.sauceCount = 3;
                    pizza.panType = 1;
                    break;
                case 2:
                    pizza.pepperoniCount = 1;
                    pizza.doughCount = 3;
                    pizza.cheeseCount = 2;
                    pizza.panType = 1;
                    break;
                case 3:
                    pizza.pineappleCount = 1;
                    pizza.cheeseCount = 2;
                    pizza.doughCount = 2;
                    pizza.sauceCount = 3;
                    pizza.panType = 1;
                    break;
                case 4:
                    pizza.cheeseCount = 1;
                    pizza.doughCount = 1;
                    pizza.sauceCount = 4;
                    pizza.panType = 1;
                    break;
                case 5:
                    pizza.sauceCount = 3;
                    pizza.pepperoniCount = 2;
                    pizza.cheeseCount = 3;
                    pizza.panType = 1;
                    break;
                case 6:
                    pizza.sauceCount = 4;
                    pizza.pineappleCount = 2;
                    pizza.cheeseCount = 4;
                    pizza.panType = 1;
                    break;
                default:
                    pizza.doughCount = 1;
                    pizza.panType = 1;
                    break;
            }
        }

        if (pansize == 2)
        {
            switch (randomInt)
            {
                case 1:
                    pizza.cheeseCount = 5;
                    pizza.doughCount = 3;
                    pizza.sauceCount = 3;
                    pizza.panType = 2;
                    break;
                case 2:
                    pizza.pepperoniCount = 2;
                    pizza.doughCount = 3;
                    pizza.cheeseCount = 4;
                    pizza.panType = 2;
                    break;
                case 3:
                    pizza.pineappleCount = 3;
                    pizza.cheeseCount = 4;
                    pizza.doughCount = 5;
                    pizza.sauceCount = 4;
                    pizza.panType = 2;
                    break;
                case 4:
                    pizza.cheeseCount = 1;
                    pizza.doughCount = 1;
                    pizza.sauceCount = 7;
                    pizza.panType = 2;
                    break;
                case 5:
                    pizza.sauceCount = 4;
                    pizza.pepperoniCount = 3;
                    pizza.cheeseCount = 4;
                    pizza.panType = 2;
                    break;
                case 6:
                    pizza.sauceCount = 5;
                    pizza.pineappleCount = 5;
                    pizza.cheeseCount = 4;
                    pizza.panType = 2;
                    break;
                default:
                    pizza.doughCount = 1;
                    pizza.panType = 2;
                    break;
            }
        }
    }


}
