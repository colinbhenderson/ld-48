﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PizzaWorkbenchController : MonoBehaviour, IDropHandler
{

    //public GameObject TimerBar;
    public GameObject InventoryManager;
    public GameObject PizzaObject;
    public GameObject PizzaParent;
    public GameObject PizzaPapa;
    public Slider slider;
    public Text PanText;
    public Text CheeseText;
    public Text SauceText;
    public Text DoughText;
    public Text PepperoniText;
    public Text PineappleText;
    public bool pizzaOnTable = false;

    private int cheeseCount;
    private int doughCount;
    private int pepperoniCount;
    private int pineappleCount;
    private int sauceCount;
    private int panType;
    private int newPizzaNum = 0;




    void Update()
    {

        //All of the values should also have a graphical update
        CheeseText.text = "Cheese Amt: " + cheeseCount;
        SauceText.text = "Sauce Amt: " + sauceCount;
        DoughText.text = "Dough Amt: " + doughCount;
        PepperoniText.text = "Pepperoni Amt: " + pepperoniCount;
        PineappleText.text = "Pineapple Amt: " + pineappleCount;



        //Determins what Pan is selected in slider
        if (slider.value == 0)
        {
            panType = 0;
            PanText.text = "Pan Type: " + "Pan";
        }
        else if (slider.value == 1)
        {
            panType = 1;
            PanText.text = "Pan Type: " + "Deep";
        }
        else if (slider.value == 2)
        {
            panType = 2;
            PanText.text = "Pan Type: " + "Deepest";
        }
        else
        {
            Debug.Log("Error updating Pan Text");
        }
    }

  
    //Adds Ingedeants to the pizza and checks that there is enough in inventory
    public void OnDrop(PointerEventData eventData)
    {
        //Gets the item that is attatched to the pointer
        RectTransform item = eventData.pointerDrag.GetComponent<RectTransform>();

        //Checks to see if a pizza is curently being made
        if (pizzaOnTable)
        {
            //Checks to see if the pointer has an item and its the correct one
            if (eventData.pointerDrag != null && itemCheck(item.name))
            {
                InventoryManager.GetComponent<InventoryManager>().RemoveItem(item.name, 1); //Removes item from Inventory
                //Add the item to the pizza
                addItemToPie(item.name);

                //Reflect that change somehow
            }
        }
        else
        {
            Debug.Log("There is no Pizza Pan Ready");
        }

        
    }

    public void NewPizza()
    {
        if (pizzaOnTable) // && PizzaObject != null
        {
            Debug.Log("Pizza Already on Table");
        }
        else
        {
            pizzaOnTable = true;
            //Do something here to make a pan apear on bench
            InstantiateNewPizza("Pizza" + newPizzaNum);
            newPizzaNum++;
        }

    }


    public void InstantiateNewPizza(string pizzaName)
    {
        GameObject temp = Instantiate(PizzaObject, PizzaParent.transform);
        temp.transform.name = pizzaName;
        temp.transform.localPosition = new Vector3(45, 15, 0);
        temp.GetComponent<RectTransform>().sizeDelta = new Vector2(12, 12);
    }
    

    public void ConfrimPizza()
    {

        //Assign the Value to the pizza Object
        PizzaParent.transform.GetChild(0).GetComponent<PizzaObject>().AddToppings(
            cheeseCount,
            sauceCount,
            doughCount,
            pepperoniCount,
            pineappleCount,
            panType
            );

        //Reset Pizza Amounts
        cheeseCount = 0;
        sauceCount = 0;
        doughCount = 0;
        pepperoniCount = 0;
        pineappleCount = 0;

        //Makes Pizza Moveable
        PizzaParent.transform.GetChild(0).GetComponent<PizzaObject>().Movable = true;
        //Moves Pizza into PizzaPapa to be used elsewhere
        PizzaParent.transform.GetChild(0).transform.SetParent(PizzaPapa.transform);
        pizzaOnTable = false;

    }

    public void ClearPizza()
    {
        pizzaOnTable = false;
        GameObject.Destroy(PizzaParent.transform.GetChild(0).gameObject);
    }

    //This insures only processed items are put onto the pizza
    private bool itemCheck(string item)
    {
        switch (item)
        {
            case ("DoughMix"):
                return false;
            case ("Cheese"):
                return false;
            case ("Pepperoni"):
                return false;
            case ("Pineapple"):
                return false;
            default:
                return true;
        }
    }

    private void addItemToPie(string itemName)
    {
        switch (itemName)
        {
            case ("DoughBall"):
                doughCount += 1;
                break;
            case ("ShreadedCheese"):
                cheeseCount += 1;
                break;
            case ("SlicedPepperoni"):
                pepperoniCount += 1;
                break;
            case ("ChoppedPineapple"):
                pineappleCount += 1;
                break;
            case ("Sauce"):
                sauceCount += 1;
                break;
            default:
                Debug.Log("Error: Cannot add item to current pizza");
                break;
        }
    }

    public void SetPizzaOnTable()
    {
        pizzaOnTable = false;
    }


}
