﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameObject Level1Dispaly;
    public GameObject Level2Display;
    public GameObject Level3Display;
    public Slider slider;
    public int Level;


    // Update is called once per frame
    void Update()
    {

        switch (Level)
        {
            case (0):
                break;
                //Do nothing becuase this is the default
            case (1):
                slider.maxValue = 1;
                Level1Dispaly.SetActive(false);
                Level2Display.SetActive(true);
                break;
            case (2):
                slider.maxValue = 2;
                Level2Display.SetActive(false);
                Level3Display.SetActive(true);
                break;
        }
    }

    public void TooLevelTwo()
    {
        Level = 1;
    }

    public void TooLevelThree()
    {
        Level = 2;
    }



}
