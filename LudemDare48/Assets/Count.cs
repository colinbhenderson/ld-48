﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Count : MonoBehaviour
{
    public int count;
    public Text text;
    public int cost;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        text.text = count.ToString();
    }

    public void addOne()
    {
        count += 1;
    }

    public void subtractOne()
    {
        if(count > 0)
        count -= 1;
    }
}
