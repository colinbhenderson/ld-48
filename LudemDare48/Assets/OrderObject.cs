﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OrderObject : MonoBehaviour, IDropHandler
{
    public int cheeseCount = 0;
    public int pepperoniCount = 0;
    public int pineappleCount = 0;
    public int doughCount = 0;
    public int sauceCount = 0;
    public int panType = 0;

    public Text cheeseText;
    public Text sauceText;
    public Text doughText;
    public Text pepperoniText;
    public Text pineappleText;
    public Text pizzaTypeText;
    public InventoryManager InventoryManager;
    public GameClockManager gameClockManager;
    
    //------------------------------------------------------
    //Graphics Updater
    void Start()
    {

        InventoryManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
        gameClockManager = GameObject.Find("Cutomer_Gameclock Manager").GetComponent<GameClockManager>();



        string pizzaType = "";

        switch (panType)
        {
            case (0):
                pizzaType = "Pan";
                break;
            case (1):
                pizzaType = "Deep";
                break;
            case (2):
                pizzaType = "Deepest";
                break;
            default:
                Debug.Log("Error Displaying Pan Type in OrderObject");
                break;
        }



        /*OrderText.text = "Cheese:  " + cheeseCount +
             "\nSauce: " + sauceCount +
             "\nDough: " + doughCount +
             "\nPep: " + pepperoniCount +
             "\nPienA: " + pineappleCount +
             "\nPan: " + pizzaType; */

        cheeseText.text = cheeseCount.ToString();
        sauceText.text = sauceCount.ToString();
        doughText.text = doughCount.ToString();
        pepperoniText.text = pepperoniCount.ToString();
        pineappleText.text = pineappleCount.ToString();
        pizzaTypeText.text = pizzaType;
    }




    void Update()
    {

    }


    //--------------------------------------------------------------
    //Makes the customer tickets be able to accept pizza

    public void OnDrop(PointerEventData eventData)
    {
        //Gets the item that is attatched to the pointer
        RectTransform item = eventData.pointerDrag.GetComponent<RectTransform>();


        //Checks to see if the pointer has an item and its the correct one
        if (eventData.pointerDrag != null && checkPizzaComp(item))
        {
            //Add Money to Account
            //TODO: WE HAVE TO RANDOMLY CALC MONEY
            InventoryManager.GetComponent<InventoryManager>().AddMoney(50);
            gameClockManager.customersServed++;
            gameClockManager.moneyMade += 50;
            GameObject.Destroy(item.transform.GetComponent<PizzaObject>().gameObject);
            GameObject.Destroy(this.gameObject);


        }
        //An item that does not work was palced here
        else
        {
            //TODO: SOMETHING TO INDICATE YOU PUT THE ITEM IN THE WRONG PLACE
            Debug.Log("Not the Right Pizza!");
        }
    }

    //Does math to see if the Pizza is correct for this order - Total of 0 = Correct
    private bool checkPizzaComp(RectTransform tempItem)
    {
        int total = this.cheeseCount - tempItem.transform.GetComponent<PizzaObject>().cheeseCount +
            this.pepperoniCount - tempItem.transform.GetComponent<PizzaObject>().pepperoniCount +
            this.pineappleCount - tempItem.transform.GetComponent<PizzaObject>().pineappleCount +
            this.doughCount - tempItem.transform.GetComponent<PizzaObject>().doughCount +
            this.sauceCount - tempItem.transform.GetComponent<PizzaObject>().sauceCount +
            this.panType - tempItem.transform.GetComponent<PizzaObject>().panType;

        Debug.Log("DoingMath: " + total);

        //Checks that all the ingredients are correct and that the pizza is cooked 
        if (total == 0 && tempItem.transform.GetComponent<PizzaObject>().IsCooked)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
