﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalcCost : MonoBehaviour
{
    public List<GameObject> supplies;
    public int totalCost;
    public Text displayCost;
    public InventoryManager inventoryManager;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        inventoryManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();
    }

    // Update is called once per frame
    void Update()
    {
        displayCost.text = totalCost.ToString();
    }

    public void calcCost()
    {
        totalCost = 0;
        foreach(GameObject item in supplies)
        {
            totalCost += item.GetComponent<Count>().count * item.GetComponent<Count>().cost;
        }
    }

    public void purchaseGoods()
    {
        if (inventoryManager.Money < totalCost)
        {
            animator.Play("PurchaseError", -1, 0f);
        }
        else
        {
            inventoryManager.RemoveMoney(totalCost);
            foreach (GameObject item in supplies)
            {
                switch (item.name)
                {
                    case "Cheese":
                        inventoryManager.Cheese += item.GetComponent<Count>().count;
                        break;
                    case "Dough":
                        inventoryManager.DoughMix += item.GetComponent<Count>().count;
                        break;
                    case "Pepperoni":
                        inventoryManager.Pepperoni += item.GetComponent<Count>().count;
                        break;
                    case "Pineapple":
                        inventoryManager.Pineapple += item.GetComponent<Count>().count;
                        break;
                    case "Sauce":
                        inventoryManager.Sauce += item.GetComponent<Count>().count;
                        break;
                    default:
                        Debug.LogError("CalcCost Couldn't find the name given to it!" + " The real name is : " + item.name);
                        break;
                }

                item.GetComponent<Count>().count = 0;
                totalCost = 0;
            }
        }
    }
}
