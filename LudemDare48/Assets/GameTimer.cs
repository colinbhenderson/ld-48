﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    void Start()
    {
        CurrentValue = 0f;
    }

    public Slider slider;
    private float totalTime = 180; //This determins the speed of the timer
    public bool TimerIsDone = false;

    private float currentValue = 0f;
    private bool timerOn = false;

    public float CurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            currentValue = value;
            slider.value = currentValue;

        }
    }




    void Update()
    {
        if (timerOn)
        {
            CurrentValue += Time.deltaTime;
        }

        if (CurrentValue >= totalTime)
        {
            timerOn = false;
            CurrentValue = 0;
            slider.gameObject.SetActive(false);
            TimerIsDone = true;
        }
    }

    public void TimerStart()
    {
        slider.gameObject.SetActive(true);
        timerOn = true;
    }
    
    public void SetTimerOff()
    {
        TimerIsDone = false;
    }

    public void SetTime(float timeScale)
    {
        totalTime = timeScale;
    }
}
